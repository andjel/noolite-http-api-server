import os
import time
import logging

import asyncio
import jinja2
import aiohttp_jinja2
import aiohttp_debugtoolbar
from aiohttp import web
from aiohttp_debugtoolbar import toolbar_middleware_factory

from noolite_api.noolite_serial import NooliteSerial


# Logging config
logger = logging.getLogger()
formatter = logging.Formatter('%(asctime)s - %(filename)s:%(lineno)s - %(levelname)s - %(message)s')
logger.setLevel('DEBUG')


def get_available_serial_port():
    if os.path.exists('/dev/ttyUSB0'):
        serial_port = '/dev/ttyUSB0'
    elif os.path.exists('/dev/tty.usbserial-AL00HDFJ'):
        serial_port = '/dev/tty.usbserial-AL00HDFJ'
    else:
        raise FileNotFoundError('USB serial port device not found')

    return NooliteSerial(serial_port, loop)


@aiohttp_jinja2.template('index.html')
async def index(request):
    return {}


async def write(request):
    uart_params = {
        'mode': 0,
        'ctr':  0,
        'res':  0,
        'ch':   0,
        'cmd':  0,
        'fmt':  0,
        'd0':   0,
        'd1':   0,
        'd2':   0,
        'd3':   0,
        'id0':  0,
        'id1':  0,
        'id2':  0,
        'id3':  0,
    }

    for attr_name, attr_value in request.GET.items():
        if attr_name in uart_params:
            uart_params[attr_name] = int(attr_value)

    before = time.time()
    print('> {}'.format(uart_params))
    response_from_usb = await noolite_serial.send_command(**uart_params)
    response_from_usb = [resp.to_list() for resp in response_from_usb]
    print('< {}'.format(response_from_usb))
    print('Time fore request: {}\n'.format(time.time() - before))
    return web.json_response({'usb_response': response_from_usb})


loop = asyncio.get_event_loop()

app = web.Application(loop=loop, middlewares=[toolbar_middleware_factory])
aiohttp_debugtoolbar.setup(app)
aiohttp_jinja2.setup(app, loader=jinja2.PackageLoader('noolite_api', 'templates'))

# Serial
noolite_serial = get_available_serial_port()

app.router.add_get('/api.htm', write)
app.router.add_get('/', index)
app.router.add_static('/', path=os.path.join(os.path.dirname(os.path.realpath(__file__)), 'static'), name='static')


def run_server():
    web.run_app(app, host='0.0.0.0', port=8080)


if __name__ == '__main__':
    run_server()
