import os
import time
import logging

import serial
import asyncio

# Logging config
logger = logging.getLogger()
formatter = logging.Formatter('%(asctime)s - %(filename)s:%(lineno)s - %(levelname)s - %(message)s')
logger.setLevel('DEBUG')


class NooLiteMessage:
    def __init__(self, ch, cmd, mode, id0, id1, id2, id3):
        self.mode = mode
        self.ch = ch
        self.cmd = cmd
        self.id0 = id0
        self.id1 = id1
        self.id2 = id2
        self.id3 = id3

    def __eq__(self, other):
        return all([
            self.mode == other.mode,
            self.ch == other.ch,
            self.id0 == other.id0,
            self.id1 == other.id1,
            self.id2 == other.id2,
            self.id3 == other.id3
        ])

    def is_id(self):
        return any([
            self.id0 != 0,
            self.id1 != 0,
            self.id2 != 0,
            self.id3 != 0
        ])


class NooLiteResponse(NooLiteMessage):
    def __init__(self, st, mode, ctr, togl, ch, cmd, fmt, d0, d1, d2, d3, id0, id1, id2, id3, crc, sp):
        super().__init__(ch, cmd, mode, id0, id1, id2, id3)
        self.time = time.time()
        self.st = st
        self.ctr = ctr
        self.togl = togl
        self.fmt = fmt
        self.d0 = d0
        self.d1 = d1
        self.d2 = d2
        self.d3 = d3
        self.crc = crc
        self.sp = sp
        self.time = time.time()

    def to_list(self):
        return [
            self.st,
            self.mode,
            self.ctr,
            self.togl,
            self.ch,
            self.cmd,
            self.fmt,
            self.d0,
            self.d1,
            self.d2,
            self.d3,
            self.id0,
            self.id1,
            self.id2,
            self.id3,
            self.crc,
            self.sp
        ]


class NooLiteCommand(NooLiteMessage):
    def __init__(self, ch, cmd, mode=0, ctr=0, res=0, fmt=0, d0=0, d1=0, d2=0, d3=0, id0=0, id1=0, id2=0, id3=0):
        super().__init__(ch, cmd, mode, id0, id1, id2, id3)
        self.st = 171
        self.ctr = ctr
        self.res = res
        self.fmt = fmt
        self.d0 = d0
        self.d1 = d1
        self.d2 = d2
        self.d3 = d3
        self.sp = 172

    @property
    def crc(self):
        crc = sum([
            self.st,
            self.mode,
            self.ctr,
            self.res,
            self.ch,
            self.cmd,
            self.fmt,
            self.d0,
            self.d1,
            self.d2,
            self.d3,
            self.id0,
            self.id1,
            self.id2,
            self.id3,
        ])
        return crc if crc < 256 else divmod(crc, 256)[1]

    def to_bytes(self):
        return bytearray([
            self.st,
            self.mode,
            self.ctr,
            self.res,
            self.ch,
            self.cmd,
            self.fmt,
            self.d0,
            self.d1,
            self.d2,
            self.d3,
            self.id0,
            self.id1,
            self.id2,
            self.id3,
            self.crc,
            self.sp
        ])

    def description(self):
        return {
            'ST':   {'value': self.st, 'description': 'Стартовый байт'},
            'MODE': {'value': self.mode, 'description': 'Режим работы адаптера'},
            'CTR':  {'value': self.ctr, 'description': 'Управление адаптером'},
            'RES':  {'value': self.res, 'description': 'Зарезервирован, не используется'},
            'CH':   {'value': self.ch, 'description': 'Адрес канала, ячейки привязки'},
            'CMD':  {'value': self.cmd, 'description': 'Команда'},
            'FMT':  {'value': self.fmt, 'description': 'Формат'},
            'DATA': {'value': [self.d0, self.d1, self.d2, self.d3], 'description': 'Данные'},
            'ID':   {'value': [self.id0, self.id1, self.id2, self.id3], 'description': 'Адрес блока'},
            'CRC':  {'value': self.crc, 'description': 'Контрольная сумма'},
            'SP':   {'value': self.sp, 'description': 'Стоповый байт'}
        }


class NooliteSerial:
    def __init__(self, tty_name, loop):
        self.tty = self._get_tty(tty_name)
        self.responses = []
        self.loop = loop

        # Если приходят данные на адаптер, то они обрабатываются в этом методе
        self.loop.add_reader(self.tty.fd, self.inf_reading)
        # Очистка старых невостребованных ответов
        self.loop.create_task(self.remove_old_responses())

    def inf_reading(self, timeout=2):
        while self.tty.in_waiting >= 17:
            resp = NooLiteResponse(*list(self.tty.read(17)))
            if not resp.is_id():
                return
            self.responses.append(resp)

            # # Check togl byte
            # start_time = time.time()
            # while (response[-1].togl != 0) and (time.time() - start_time < timeout):
            #     if self.tty.in_waiting >= 17:
            #         response.append(NooLiteResponse(*list(self.tty.read(17))))
            #
            # self.responses.append(response)

    async def remove_old_responses(self):
        while True:
            responses_to_delete = []
            for resp_index, resp in enumerate(self.responses):
                if time.time() - resp.time > 10:
                    responses_to_delete.append(resp_index)
            [self.responses.pop(i) for i in responses_to_delete]
            await asyncio.sleep(10)

    async def send_command(self, ch, cmd, mode=0, ctr=0, res=0, fmt=0, d0=0, d1=0, d2=0, d3=0, id0=0, id1=0, id2=0, id3=0):
        command = NooLiteCommand(ch, cmd, mode, ctr, res, fmt, d0, d1, d2, d3, id0, id1, id2, id3)

        # Write
        before = time.time()
        self.tty.write(command.to_bytes())
        print('Time to write: {}'.format(time.time() - before))

        # Режимы работы со старыми адаптерами. Ответ приходит всегда один и тотже, так как нет обратной связи
        # Поэтому можно не тратить время на его чтения с адаптера, а формировать самостоятельно
        if command.mode in [0, 1]:
            response = [NooLiteResponse(173, command.mode, command.ctr, 0, command.ch, command.cmd, command.fmt,
                                        command.d0, command.d1, command.d2, command.d3, 0, 0, 0, 0, 175, 174)]

        else:
            # Read
            read_before = time.time()
            response = await self.get_response(command, 2)
            print('Time to read: {}'.format(time.time() - read_before))
            logger.info('< {}'.format(response))

        return response

    async def get_response(self, command, timeout):
        start_time = time.time()
        result = []
        while time.time() - start_time < timeout:
            if command.cmd == 130:
                if self.responses:
                    resp = self.responses.pop(0)
                    result.append(resp)
                    if resp.togl == 0:
                        return result
            else:
                for resp_index, resp in enumerate(self.responses):
                    if resp == command:
                        result = self.responses.pop(resp_index)
                        return [result]
            await asyncio.sleep(0)
        return []

    @staticmethod
    def _get_tty(tty_name):
        serial_port = serial.Serial(tty_name, timeout=2)
        if not serial_port.is_open:
            serial_port.open()
        serial_port.flushInput()
        serial_port.flushOutput()
        return serial_port
