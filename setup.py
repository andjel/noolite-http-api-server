from setuptools import setup, find_packages


def readme():
    with open('README.md', 'r', encoding='utf-8') as f:
        return f.read()

setup(
    name='noolite_api',
    description='HTTP API for MTRF NooLite.',
    url='https://bitbucket.org/jenrus/noolite-http-api-server',
    version='0.1.0',
    long_description=readme(),
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'aiohttp==1.1.6',
        'aiodns==1.1.1',
        'aiohttp-debugtoolbar==0.3.0',
        'Jinja2==2.8',
        'aiohttp-jinja2==0.12.0',
        'cchardet==1.1.1',
        'pyserial==3.2.1',
    ],
    entry_points={
        'console_scripts': ['noolite_serve=noolite_api.server:run_server'],
    }
)
